package com.curso.microservices.app.cursos.services;

import com.curso.microservices.app.cursos.models.entity.Curso;
import com.curso.microservices.commons.services.CommonService;

public interface CursoService extends CommonService<Curso>{
	
	public Curso findByCursoByAlunoId(Long id);
	
	public Iterable<Long> obterExamesIdComRespostasAluno(Long alunoId);
}
