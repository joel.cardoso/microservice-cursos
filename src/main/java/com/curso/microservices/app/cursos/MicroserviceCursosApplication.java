package com.curso.microservices.app.cursos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableEurekaClient
@SpringBootApplication
@EntityScan({"com.curso.microservices.commons.alunos.models.entity", 
			"com.curso.microservices.app.cursos.models.entity", 
			"com.curso.microservices.commons.exames.models.entity"})
public class MicroserviceCursosApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceCursosApplication.class, args);
	}

}
