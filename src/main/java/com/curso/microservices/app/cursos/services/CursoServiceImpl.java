package com.curso.microservices.app.cursos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.curso.microservices.app.cursos.clients.RespostaFeignClient;
import com.curso.microservices.app.cursos.models.entity.Curso;
import com.curso.microservices.app.cursos.models.repository.CursoRepository;
import com.curso.microservices.commons.services.CommonServiceImpl;

@Service
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursoService{
	
	@Autowired
	private RespostaFeignClient client;
	
	@Transactional(readOnly = true)
	@Override
	public Curso findByCursoByAlunoId(Long id) {
		return repository.findByCursoByAlunoId(id);
	}

	@Override
	public Iterable<Long> obterExamesIdComRespostasAluno(Long alunoId) {
		return client.obterExamesIdComRespostasAluno(alunoId);
	}

}
