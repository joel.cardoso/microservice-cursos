package com.curso.microservices.app.cursos.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.curso.microservices.app.cursos.models.entity.Curso;
import com.curso.microservices.app.cursos.services.CursoService;
import com.curso.microservices.commons.alunos.models.entity.Aluno;
import com.curso.microservices.commons.controllers.CommonController;
import com.curso.microservices.commons.exames.models.entity.Exame;

@RestController
public class CursoController extends CommonController<Curso, CursoService>{
	
	@PutMapping
	public ResponseEntity<?> update(@Valid @RequestBody Curso curso, BindingResult result, @PathVariable Long id) {
		
		if (result.hasErrors()) {
			return this.validate(result);
		}
		
		Optional<Curso> cursoOptional = this.service.findById(id);
		if (!cursoOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Curso cursoSaved = cursoOptional.get();
		cursoSaved.setNome(curso.getNome());
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoSaved));
	}
	
	@PutMapping("/{id}/adicionar-alunos")
	public ResponseEntity<?> addAlunos(@RequestBody List<Aluno> alunos, @PathVariable Long id) {
		Optional<Curso> cursoOptional = this.service.findById(id);
		if (!cursoOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Curso cursoSaved = cursoOptional.get();
		alunos.forEach(a -> {
			cursoSaved.addAlunos(a);
		});
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(cursoSaved));
	}
	
	@PutMapping("/{id}/remover-aluno")
	public ResponseEntity<?> removeAluno(@RequestBody Aluno aluno, @PathVariable Long id) {
		Optional<Curso> cursoOptional = this.service.findById(id);
		if (!cursoOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Curso cursoSaved = cursoOptional.get();
		cursoSaved.removeAlunos(aluno);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(cursoSaved));
	}
	
	@GetMapping("/alunos/{id}")
	public ResponseEntity<?> findByAlunoId(@PathVariable Long id) {
		Curso curso = service.findByCursoByAlunoId(id);
		
		if (curso != null) {
			List<Long> examesIds = (List<Long>) service.obterExamesIdComRespostasAluno(id);
			
			List<Exame> exames = curso.getExames().stream().map(exame -> {
				if (examesIds.contains(exame.getId())) {
					exame.setRespondido(true);
				}
				return exame;
			}).collect(Collectors.toList());
			
			curso.setExames(exames);
		}
		return ResponseEntity.ok(curso);
	}
	
	@PutMapping("/{id}/adicionar-exames")
	public ResponseEntity<?> addExames(@RequestBody List<Exame> exames, @PathVariable Long id) {
		Optional<Curso> cursoOptional = this.service.findById(id);
		if (!cursoOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Curso cursoSaved = cursoOptional.get();
		exames.forEach(e -> {
			cursoSaved.addExame(e);
		});
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(cursoSaved));
	}
	
	@PutMapping("/{id}/remover-exames")
	public ResponseEntity<?> removeExames(@RequestBody Exame exame, @PathVariable Long id) {
		Optional<Curso> cursoOptional = this.service.findById(id);
		if (!cursoOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Curso cursoSaved = cursoOptional.get();
		cursoSaved.removeExame(exame);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(cursoSaved));
	}
}
