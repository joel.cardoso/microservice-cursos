package com.curso.microservices.app.cursos.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="microservice-respostas")
public interface RespostaFeignClient {
	
	@GetMapping("/aluno/{alunoId}/exames-respondidos")
	public Iterable<Long> obterExamesIdComRespostasAluno(@PathVariable Long alunoId);
}
