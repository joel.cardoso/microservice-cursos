package com.curso.microservices.app.cursos.models.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.curso.microservices.app.cursos.models.entity.Curso;

@Repository
public interface CursoRepository extends PagingAndSortingRepository<Curso, Long>{
	
	@Query("select c from Curso c join fetch c.alunos a where a.id=?1")
	public Curso findByCursoByAlunoId(Long id);
}
