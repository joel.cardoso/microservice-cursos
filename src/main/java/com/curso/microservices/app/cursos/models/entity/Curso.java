package com.curso.microservices.app.cursos.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import com.curso.microservices.commons.alunos.models.entity.Aluno;
import com.curso.microservices.commons.exames.models.entity.Exame;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "cursos")
public class Curso implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String nome;
	
	@Column(name = "create_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createAt;
	
	@JsonIgnoreProperties(value = {"curso"}, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "curso", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CursoAluno> cursoAlunos;
	
	//@OneToMany(fetch = FetchType.LAZY)
	@Transient
	private List<Aluno> alunos;
	
	@ManyToMany(fetch = FetchType.LAZY)
	private List<Exame> exames;
	
	@PrePersist
	public void prePersist() {
		this.createAt = new Date();
	}

	public Curso() {
		this.alunos = new ArrayList<>();
		this.exames = new ArrayList<>();
		this.cursoAlunos = new ArrayList<>();
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}
	
	public void addAlunos(Aluno aluno) {
		this.alunos.add(aluno);
	}
	
	public void removeAlunos(Aluno aluno) {
		this.alunos.remove(aluno);
	}

	public List<Exame> getExames() {
		return exames;
	}

	public void setExames(List<Exame> exames) {
		this.exames = exames;
	}
	
	public void addExame(Exame exame) {
		this.exames.add(exame);
	}
	
	public void removeExame(Exame exame) {
		this.exames.remove(exame);
	}

	public List<CursoAluno> getCursoAlunos() {
		return cursoAlunos;
	}

	public void addCursoAluno(CursoAluno cursoAluno) {
		this.cursoAlunos.add(cursoAluno);
	}
	
	public void removeCursoAluno(CursoAluno cursoAluno) {
		this.cursoAlunos.remove(cursoAluno);
	}
	
}
